const express = require('express')
const app =  express();
const port = 4000;
const data = require('./MOCK_DATA.json')
const Tree = require('./Classes/Tree')

const trees = {}

const filterBy = (value, property)  => {

    if(!trees[property]) {
        const tree = new Tree();
        data.forEach((element, index) => {
            tree.add(element, property)
        })
        trees[property] = tree
    }
    const element = trees[property].findRecursive(value)
    console.log(element)
    return element;
}

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });

app.get('/', (request, response) => {
    console.log(data);
    response.send(data)
})

app.get('/:prop/:value', (request, response) => {
    
    const property = request.params.prop
    if( property === 'id' || 
        property === 'modelo' || 
        property === 'precio' || 
        property === 'cilindrada') {
        
    } else {
        return response.send({
            error: 'propiedad no idexada'
        })
    }
    const value = request.params.value

    const filtered = data.filter(motor => {
        return motor[property] == value
    })

    response.send(filterBy(value, property).items)
})

app.listen(port , () => {
    console.log(port)
})