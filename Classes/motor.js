// Clase Motor, no se aún si es necesario agregar
// mas clases o no, pero esta clase sera la base 
// para generar los datos y utilizar el buscador.

class Motor {
    id;
    marca;
    color;
    tipo;
    modelo;
    cilindrada;
    precio;

    constructor(motor) {
        this.id = motor.id;
        this.marca = motor.marca;
        this.color = motor.color;
        this.tipo = motor.tipo;
        this.modelo = motor.modelo;
        this.cilindrada = motor.cilindrada;
        this.precio = motor.precio;
    }

    busqueda() {
        return null;
    }
}

module.export  = {
    Motor
}

