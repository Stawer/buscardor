function quicksort(num) {
    if (!num) {
        num = 10;
    }
    let array = [];
    for (let i=0; i<num; i++) {
        array.push(Math.ceil(Math.random()*100));
    }
    
    let sorted = sort(array);
}

function sort(array) {
    // Condicion de corte de funcion recursiva
    if (array.length <= 1) {
        return array;
    } else if (array.length === 2) {
        if (array[1] < array[0]) {
            return[array[0], array[1]];
        } 
        return array;
    }

    let pivot = array[Math.floor(array.length / 2)];

    let right = [];
    let center = [];
    let left = [];

    for (let i=0; i < array.length; i++) {
        if (array[i] < pivot) {
            left.push(array[i]);
        } else if(array[i] === pivot){
            center.push(array[i]);
        } else {
            right.push(array[i]);
        }
    }

    let finalLeft = sort(left);
    let finalRight = sort(right);

    if (finalLeft.length === 0) {
        return center.concat(finalRight)
    }

    return [...finalLeft, ...center, ...finalRight];
}

quicksort(process.argv[2]);

module.exports = {
    sort
}