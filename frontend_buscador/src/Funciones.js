import React from "react";
import Service from './services/Service'
import data from './MOCK_DATA'
import Quicksort from './services/Quicksort'

const service = new Service(data);

export default class extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            list: []
        }
        this.onSubmit.bind(this.onSubmit)
    }

    render() {
        const { list }= this.state
        return <div>
            <div>Creación</div>
            <form onSubmit={this.onCreate}>
                <input name="marca" placeholder="marca"/>
                <input name="modelo" type="number" placeholder="modelo"/>
                <input name="color"  placeholder="color"/>
                <input name="tipo"  placeholder="tipo"/>
                <input name="cilindrada"  placeholder="cilindrada"/>
                <input name="precio"  placeholder="precio"/>
                <button type="submit">Crear</button>
            </form>
            <div>Busqueda</div>
            <form onSubmit={this.onSubmit}>
                <input name="value" placeholder="valor"/>
                busqueda por:
                <select name="property">
                    <option value="cilindrada">cilindrada</option>
                    <option value="precio">precio</option>
                    <option value="modelo">modelo</option>
                </select>
                ordenar por
                <select name="order">
                    <option value="cilindrada">cilindrada</option>
                    <option value="precio">precio</option>
                    <option value="modelo">modelo</option>
                </select>
                <button type="submit">Buscar</button>
                <ul styles="list-style: none">
                    {list.length ? list.map(item => {
                        return (
                            <li 
                                key={item.id} 
                                styles="color:white">
                                Marca: {item.marca}, 
                                Modelo: {item.modelo}, 
                                Color: {item.color}, 
                                Tipo: {item.tipo},
                                {item.cilindrada} ml, 
                                ${item.precio}
                            </li>
                        )
                    }): "Sin resultados"}
                </ul>
            </form>
            
        </div>
    }


    onSubmit = (event) => {
        event.preventDefault();
        const value = event.target['value'].value;
        const property = event.target['property'].value;
        const order = event.target['order'].value;
        console.log(value, property, order)

        const list = service.filterBy(value, property).items

        const orderFunc  = Quicksort(list, (a, b) => {
            if (a[order] < b[order]) {
              return -1;
            }
            if (a[order] > b[order]) {
              return 1;
            }
            return 0;
        })

        if(!value) {
            this.setState( _ => {
                return {
                    list: orderFunc
                }
            })
        } else {
            this.setState( _ => {
                return {
                    list: orderFunc
                }
            })
        }
        
    }

    onCreate = (event) => {
        event.preventDefault();
        const marca = event.target['marca'].value;
        const modelo = event.target['modelo'].value;
        const color = event.target['color'].value;
        const tipo = event.target['tipo'].value;
        const precio = event.target['precio'].value;
        const cilindrada = event.target['cilindrada'].value;

        const item = {
            marca,
            modelo,
            color,
            tipo, 
            cilindrada,
            precio
        }

        service.addItem(item)
        
    }

}
