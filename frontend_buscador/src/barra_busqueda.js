import React from "react";
import Service from './services/Service'
import data from './MOCK_DATA'
const service = new Service(data);

class BarraBusqueda extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            list: []
        }
        this.onSubmit.bind(this.onSubmit)
    }
    render() {
        const {list}= this.state
        return <form onSubmit={this.onSubmit}>
            <input name="value"/>
            <input name="property"/>
            <button type="submit">Picooo!</button>
            <ul styles="list-style: none">
                {list.map(item => {
                    console.log(item)
                    return <li 
                    key={item.id} 
                    styles="color:white">{item.marca}, {item.modelo}, {item.cilindrada}, ${item.precio}</li>
                })}
            </ul>
        </form>;
    }


    onSubmit = (event) => {
        event.preventDefault();
        const value = event.target['value'].value;
        const property = event.target['property'].value;

        const list = service.filterBy(value, property).items
        console.log(list)
        this.setState( _ => {
            return {
                list
            }
        })
    }

}

export default BarraBusqueda;
