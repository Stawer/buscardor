import Tree from './Tree'

export default class {
    
    constructor(data) {
        this.data = data;
        this.trees = {};
    }

    filterBy (value, property)  {

        //genera el arbol por propiedad si no existe
        if(!this.trees[property]) {
            const tree = new Tree();
            this.data.forEach((element, index) => {
                tree.add(element, property)
            })
            this.trees[property] = tree
        }

        //realiza la busqueda recursiva en el arbol por la propiedad
        const element = this.trees[property].findRecursive(value)
        console.log(this.trees)
        if(!element) {
            return {
                items: []
            }
        }
        return element;
    }

    addItem(item) {
        //añade un elemento al final de data y resetea los arboles
        this.data.push(item)
        this.trees = {}
    }

}