// Buscamos el primer numero del tag
// Si es Gato
// [71, 97, 116, 111]
// Object = 'http://htttp.cat'

// 1era iteracion

// letter = G
// tag = ato
// Object = Cualquier cosa o lo que queremos indexar

// 2era iteracion

// letter = a
// tag = to
// Object = Cualquier cosa o lo que queremos indexar

// 3era iteracion

// letter = t
// tag = o
// Object = Cualquier cosa o lo que queremos indexar

// 4ta iteracion

// letter = o
// tag = 
// Object = Cualquier cosa o lo que queremos indexar

// Si la 4ta condicion ocurre
// Entonces debemos adjuntar el object que traemos para indexar.

// Object = ['http://htttp.cat'];
