function tree() {
  this.node = undefined;
  this.add = function (tag, object) {
    console.log('=========' + tag + '========');
    // Buscamos el primer numero del tag
    // Si es Gato
    // [71, 97, 116, 111]
    let letter = tag.charAt(0);
    tag = tag.substring(1, tag.length);

    // letter = G
    // tag = ato
    // Object = Cualquier cosa o lo que queremos indexar

    if (!this.node) {
      this.node = new node(letter, tag, object, true);
    } else {
      const newNode = new node(letter, tag, object);
      this.node.index(newNode, tag, object);
    }
  }

  this.search = function (id) {
    return this.node.search(id);
  }
}

function node(id, tag, value, main) {
  this.left = undefined;
  this.right = undefined;
  this.objetos = (value) ? [value] : [];
  this.id = id.charCodeAt(0); // Primera iteracion G -> 71
  this.tree = undefined;

  this.deepTree = function(tag) {
    console.log(tag);
    let letter = tag.charAt(0);
    tag = tag.substring(1, tag.length);
    this.tree = new node(letter, tag, value, true);
  }

  if (main && tag && tag.length > 0) {
    this.deepTree(tag);
  }

  this.index = function (node, tag, object) {
    // Si tiene el mismo id no debe ser indexado
    // porque es el mismo elemento
    if (node.id === this.id) {
      // Si existe un arbol
      // Si ademas el tag existe
      // Si ademas el tag es mayor que cero en largo
      // Debemos indexar o crear el arbol;
      // tag = lo
      if (tag && tag.length > 0) {
        if (this.tree) {
          node.id = tag.charCodeAt(0);
          this.tree.index(node, tag, object);
        } else {
          tag = tag.substring(1, tag.length);
          node.deepTree(tag);
          this.tree = node;
        }
      } else {
        // Aca se asume que encoentramos el nodo final
        this.objetos.push(object);
      }
      return;
    }
    // En caso contrario
    if (node.id < this.id) {
      // Si el nodo izquierdo existe lo indexamos
      // En caso contrario lo creamos
      if (!this.left) {
        node.deepTree(tag);
        this.left = node;
      } else {
        tag = tag.substring(1, tag.length);
        node.id = tag.charCodeAt(0);
        this.left.tree.index(node, tag, object);
      }
    } else {
      // Si el nodo izquierdo existe lo indexamos
      // En caso contrario lo creamos
      if (!this.right) {
        node.deepTree(tag);
        this.right = node;
      } else {
        tag = tag.substring(1, tag.length);
        node.id = tag.charCodeAt(0);
        this.right.tree.index(node, tag, object);
      }
    }
  }

  this.search = function (id) {
    if (this.id === id) {
      return this;
    } else {
      if (id < this.id) {
        if (this.left) {
          return this.left.search(id);
        }
      } else {
        if (this.right) {
          return this.right.search(id);
        }
      }
      return null;
    }
  }
}

let arbolito = new tree();
arbolito.add('Gato', 'http://http.cat');
arbolito.add('Perro', 'http://http.dog');
arbolito.add('Pelo', 'http://perdielpelo.com');
arbolito.add('Pelotazo', 'http://perdielpelo.com');

console.log(arbolito.search('Pelo'.charCodeAt(0)))