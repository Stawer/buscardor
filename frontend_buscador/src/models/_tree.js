function tree() {
    this.node;
    this.add = function(num, object) {
        // Creamos el nodo que puede ser un tronco, una rama o una hoja
        if (!this.node) {
            this.node = new node(num, 1, object);
        } else {
            const newNode = new node(num, 2, object);
            this.node.index(newNode, object);
        }
    }

    this.describe = function() {
        this.node.describe();
    }

    this.search = function(id) {
        return this.node.search(id);
    }
}

function node(id, deep, object) {
    this.left; 
    this.right;
    this.deep = deep;
    this.objetos = (object) ? [object] : [];
    this.id = id;

    this.index = function(node, object) {
        // Si tiene el mismo id no debe ser indexado
        // porque es el mismo elemento
        if (node.id === this.id) {
            this.objetos.push(object);
            return;
        }
        // En caso contrario
        if (node.id < this.id) {
            // Si el nodo izquierdo existe lo indexamos
            // En caso contrario lo creamos
            if (!this.left) {
                this.left = node;
            } else {
                node.deep++;
                this.left.index(node, object);
            }
        } else {
            // Si el nodo izquierdo existe lo indexamos
            // En caso contrario lo creamos
            if (!this.right) {
                this.right = node;
            } else {
                node.deep++;
                this.right.index(node, object);
            }
        }
    }

    this.describe = function() {
        if (this.left) this.left.describe();
        if (this.right) this.right.describe();
    }

    this.search = function(id) {
        if (this.id === id) {
            return this;
        } else {
            if (id < this.id) {
                if (this.left) {
                    return this.left.search(id);
                }
            } else {
                if (this.right) {
                    return this.right.search(id);
                }
            }
            return null;
        }
    }
}

let arbolito = new tree();
for (let i=0; i< 100000; i++) {
    console.log(`indexando elemento ${i+1}`)
    arbolito.add(Math.ceil(Math.random()*1000000));
}
arbolito.add(4, 'perro');
arbolito.add(4605, 'patata');
arbolito.add(4456, 'alimentos');
arbolito.add(786, {'sdsds': 5});
arbolito.add(456, 9999);
arbolito.add(87982, ['gato1', 'gato2', 'gatop3']);
arbolito.add(4605, 'coliflor');
arbolito.add(4605, 'veterraga');
arbolito.add(4605, {'animal':5, 'props':'lol'});

let se = arbolito.search(4605);

console.log(se.objetos);
console.log(se.deep);