function radixsort(num) {
    if (!num) {
        num = 10;
    }
    let array = [];
    for (let i=0; i<num; i++) {
        array.push(Math.ceil(Math.random()*100));
    }

    let p = sort(array, 0, null);
}

function sort(array, index, to) {
    if (index === to) {
        return array;
    }

    let max;
    let sorted = [[],[],[],[],[],[],[],[],[],[]];

    for (let i=0; i < array.length; i++) {
        let numero = array[i].toString();
        if (numero.length < (index + 1)) {
            sorted[0].push(array[i]);
            continue;
        }
        let minux = numero.charAt(numero.length - (index + 1));

        sorted[parseInt(minux)].push(array[i]);
        if (!to && (max === undefined || array[i] > max)) {
            max = array[i];
        }
    }

    if (max != undefined) {
        to = max.toString().length;
    }

    array = [];
    for (let l = 0; l< 10; l++) {
        array = [...array, ...sorted[l]];
    }

    return sort(array, ++index, to);
}

radixsort(process.argv[2]);