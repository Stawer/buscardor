import Node from "./Node";

class Tree {
	constructor() {
		this.node = undefined;
	}

	add(tag, object) {
		console.log("=========" + tag + "========");
		// Buscamos el primer numero del tag
		// Si es Gato
		// [71, 97, 116, 111]
		let letter = tag.charAt(0);
		tag = tag.substring(1, tag.length);

		// letter = G
		// tag = ato
		// Object = Cualquier cosa o lo que queremos indexar

		if (!this.node) {
			this.node = new Node(letter, tag, object, true);
		} else {
			const newNode = new Node(letter, tag, object);
			this.node.index(newNode, tag, object);
		}
	}

	search(id) {
		return this.node.search(id);
	}
}

export default Tree
