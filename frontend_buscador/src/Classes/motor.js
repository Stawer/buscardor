// Clase Motor, no se aún si es necesario agregar
// mas clases o no, pero esta clase sera la base 
// para generar los datos y utilizar el buscador.

export default class {
    id;
    marca;
    color;
    tipo;
    modelo;
    cilindrada;
    precio;

    constructor(id,marca,color,tipo,modelo,cilindrada,precio) {
        this.id = "";
        this.marca = "";
        this.color = "";
        this.tipo = "";
        this.modelo = "";
        this.cilindrada = "";
        this.precio = null;
    }

    //Funciona para realizar pruebas con el buscador
    //busqueda() {
    //   return null;
    //}
}

//export default Motor;